# Vision Document
Edward Ettesvoll, Sebastian Hestsveen, Anatoli Maroz, Matias Nordli, Frederik Simonsen


[[_TOC_]]

# Revision history
| Date           | Version | Description
| -------------- | ------- | -----------
|  3 March, 2021 | 0.1     | Initial draft
|  4 March, 2021 | 0.2     | Improvements after feedback - second draft
| 11 March, 2021 | 0.3     | Improvements after feedback - third draft
| 19 March, 2021 | 0.4     | Improvements after feedback - fourth draft
|  8 April, 2021 | 0.5     | Improvements after feedback - fifth draft


# 1. Introduction
This document defines and analyzes needs and features of our software. It provides an overview of different objectives, users, product features and documentation requirements, and outlines the requirements and specifications needed by the stakeholders.

## 1.1 Purpose
The purpose of this document is to define the vision for our program, which we have decided to name ‘Quillosaur’. This includes finding out the needs of the application, in addition to the needs it is covering.

## 1.2 Scope
The scope of this document is defined by the structure and functions of our product. It includes the design, requirements and implementation of our program.


# 2. Positioning

## 2.1 Business opportunity
The client has requested a to-do-list application where users can register new tasks in the application. The application has to keep persistent data in local files. Users will have the option to enter different metadata for each task, and have a structured overview over tasks they would like to do or accomplish. With this project we are trying to meet the needs of a broad target group which would like to add more structure and organization to their daily routines. We envision that the application can be used by adults of ages 20-65.  The users could either be students, part of the work force, or someone who would like to organize their personal life only. The product will meet the needs of a broad target group which could result in a growth reputation-wise and economically.

## 2.2 Problem statement
The user has a disorganized structure for their tasks which makes it hard to remember them. This could be solved by using an app that gathers all their tasks in a clearer structure.

The user forgets a task which might result in it being done too late or never being done at all. The application will notify the user if their task has a ‘deadline’ that soon expires.

The user does not remember if their tasks are completed which could result in them spending unnecessary time figuring out if it is done. The app can solve this by having a feature that makes the user mark a task as done.

## 2.3 Product position statement
    For             Adults who would like to better organize their everyday life
    Who             Share an interest in organizing tasks
    Our program     Is a task manager program
    That            Provides a to-do-list for creating new tasks, organizing tasks, and completing tasks
    Unlike          Other task management applications that have a less intuitive user interface
    Our product     Provides the user with a task management software. The user can add new tasks, categorize, reorder and complete already registered tasks. All data from tasks will be kept persistent in local text files. It works as a standalone C++ program, for easy task management and is text-based.


# 3. Project goals

## 3.1 Efficiency goals
* Reach a broad target group and help them organize their everyday lives. Increased productivity by organizing peoples’ tasks.
* Earning a good reputation in a large user group, which can be valuable for future software development.
* Improved customer loyalty by delivering a user-friendly and efficient program.

## 3.2 Result goals
* Create a to-do-list application which can be used for registering new tasks, reordering tasks, changing the priority of tasks and finishing registered tasks.
* Make the application a standalone C++ application and keep persistent data in local files.
* Introduce the first version of the vision document, early wireframes and the first version of the Domain Model on March 7th, 2021.
* Introduce the minimum viable product and the first version of the requirements documentation WIKI on April 11th, 2021.
* Introduce the finished product design, code, and corresponding documentation on April 30th, 2021.
* Reach a cost result no higher than 10 % above the cost estimates.

## 3.3 Process goals
* Through practical experience we would like to achieve a good understanding of software development and project work. By developing our knowledge, we will be able to describe the basic concepts of these fields to * others.
* Respect every deadline presented.
* Focus on delivering a program that will satisfy the customer’s needs and demands, and in return earn top grades on our project.


# 4. Stakeholders and users

## 4.1 User environment
The program can be used with a compiler on computers. It will be compatible with Windows. There is a person involved in using the program. Our program does not need to integrate with other applications as it is a standalone C++ program.

## 4.2 Stakeholder profiles

    Client
    Representative      Seyed Ali Amirshahi
    Description         The client is a project supervisor that has requested a to-do-list application.
    Type                Business
    Responsibilities    Project supervision.
                        Review documentation, processes and software.
                        Approving changes beyond what is defined in the vision document.
                        Grading final product.
    Success criteria    Receive a program that meets the specified requirements.
                        Keeping project costs within or below the formalized budget.
                        Receiving all iterations and final product on given deadlines.
    Involvement
                        Project Management
                        Documentation review
                        Modelling and design review
                        Process review
                        Software review
    Deliverables
                        First iteration: First version of the vision document, wireframes and first version of the domain model.
                        Second iteration: Minimum viable product and the first version of requirements documentation WIKI.
                        Final iteration: Finished product including attachments and documentation.
    Comments / Issues
                        Deadlines not being respected.
                        Exceeding original budget.
                        Acquiring a program that does not meet the requirements stated in the project description.


    Development Team
    Representative          E. Ettesvoll, S. Hestsveen, A. Maroz, M. Nordli, F. Simonsen
    Description             The developers are a team of software consultants and engineers.
    Type                    Development team
    Responsibilities        Project planning and development
                            Performing user tests on target group
                            Documentation
                            Programming
                            Presentation
    Success criteria        Listen to feedback from user tests and make required changes to the program.
                            Do not exceed the budget in terms of costs.
                            Make a program that meets the requirements specified in the project description.
                            Deliver all iterations and final product within the requested deadlines.
    Involvement             Wireframes 
                            Managing user tests
                            Requirements specification
                            Modelling
                            Software configuration, design, and implementation
                            Documentation
    Deliverables
    
    Comments / Issues




## 4.3 User profiles
| Actual users     |                                                                                |
| ---------------- | ------------------------------------------------------------------------------ |
| Representative   | Stakeholder: Development team                                                  |
| Description      | Adults who would like to organize their daily tasks systematically             | 
| Type             | Students, employees, casual users                                              |
| Responsibilities | None                                                                           |
| Success criteria | Use the application to add tasks, categorize tasks and change status of tasks. |
| Involvement      | Not involved in development process                                            |

| Test users       |                                                          |
| ---------------- | -------------------------------------------------------- |
| Representative   | Family and friends of dev. team                          |
| Description      | Users that test different iterations of the wireframes.  |
| Type             | The test users vary in age, technical background and knowledge so that the application will be appropriate for a broader target group. |
| Responsibilities | Answering surveys, testing program functionality in MVP. |
| Success criteria | Finishing user tests and providing valuable feedback.    |
| Involvement      | Testing of wireframe and program.                        |


## 4.4 Key stakeholder or user needs
| Need                                | Priority | Concerns | Current Solution
| ----------------------------------- | -------- | -------- | ----------------
| Register a task                     | High     | Storing tasks in the program | Constructors have been added to the code, so tasks can be stored in the task-list both from file and run-time (solved)
| Finish tasks                        | High     | Completing registered tasks | Finishing a task can be done either upon registering a new task (mark as finished), or by changing the status of an existing task to finished (solved).
| Change the priority of tasks        | Medium   | Changing the priority of a registered task in the interval of low, medium, high | Functionality for changing the priority of a task has been implemented, and the user also has an option to regret changing a priority by entering invalid input (solved).
| Reorder tasks                       | Medium   | Ensuring that a task is moved to a requested position, and that original task and all subsequent tasks are moved one position back in the list. | As long as there are tasks registered, all tasks are displayed to the user. Users are prompted to enter which tasks they would like to move, and to which position they would like to move it to. The desired task is then moved to the correct position in the list, and new results can be seen when displaying all tasks (solved)
| Keep persistent data in local files | High     | Keeping persistent object storage for users | Text file functionality has been implemented in the program, and all tasks are both read and written to file upon execution and termination of program (solved).
| Change category of a task           | Medium   | Change the category of a task in the interval of work, home, social, hobby | The program now has functionality for reading a valid task number, read a new category for this task (can also choose same category as before to avoid changing category), and the program confirms to the user that the category has been updated (solved)
| Display all tasks to the user       | High     | Display all tasks to user in order of priority | As long as there are registered tasks in the program, all tasks are now displayed with their metadata. If there are no tasks registered, it displays an error message (solved)
| Remove a task                       | Medium   | Removing a task from the program and text file | The program asks the user which task they would like to remove from the list (they can regret and abort removing a task), if not the specified task is removed as requested (solved)

## 4.5 Alternatives and competition
**Todoist**: A task management application. Users can add new tasks, set up recurring tasks and have an overview of all tasks in a task view. Allows the user to create subtasks, change priorities and gives the user an overview of their tangible progress. Can be used both individually and in groups. Allows for synchronization with other programs such as Dropbox and Google Calendar.

**TickTick**: A task management application. Works on multiple platforms and allows for collaboration with others. Their main features allow a user to organize their everyday life, set reminders for tasks and have flexible calendar views. Can be used both individually and for collaboration.

**Microsoft** To Do: A task management application. Functions as a smart daily planner, cross-platform and allows both individual and group submissions. Integrates with Outlook Tasks and allows users to access their to-do-list both from desktops and mobile phones. 


# 5. Product overview

## 5.1 Product perspective
The system is not a self-contained program, and the user needs a C++ compiler to be able to run the program.

![User device](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/raw/master/Vision%20Document/productPerspective.JPG "User device")

## 5.2 Summary of capabilities
The capabilities of the program will be the ability for the user to add new tasks, edit tasks, delete tasks, finish tasks, see registered tasks and quit the program.

## 5.3 Risk analysis
This risk analysis documents different conditions or events that may prevent the project from succeeding. It is set by the development team’s own assumptions in regards to undesirable events that may occur during the project. The risk scenarios are represented by unique IDs.

**ID 1 - Lack of cooperation**: This includes conflicts internally in the team as well as team members’ inability to maintain active communication. Conflicts should be handled quickly and therefore would have no more than moderate impact under normal circumstances. However, it could potentially have greater impact if the team is not able to resolve the conflict. We have addressed ways to mitigate this in the collaboration agreement.

**ID 2 - Unavailable test users**: This is somewhat unlikely, around low to medium probability, but nonetheless something that would be of concern for the project as a whole. Due to its low chance however, we do not consider this to be of any more than a medium risk overall. Ways to prevent this would be to increase user test ‘scope’, so that we attempt to look for further ways (more people) to perform user tests on.

**ID 3 - Team members getting sick**: 
In general this would have no more than a moderate impact. However, it could potentially result in more severe consequences if many in the team get sick at the same time, or if the sickness is more serious than a normal cold. Given the ongoing pandemic, we have assessed the probability of sickness in the team to be high.

**ID 4 - Loss of data due to lack of backup**: 
Loss of source code, documentation, and other data related to the project could have disastrous consequences if the lost data is particularly important and not backed up. The probability of this happening is quite low however. It should also be noted that the impact could vary depending on when in the process the data loss would occur, but in general, it poses one of the greatest risks to the project.

**ID 5 - Unable to work/deliver due to technical issues**: 
One or more of the services used, such as Blackboard or GitLab, could potentially have ‘down status’ due to technical issues, resulting in the team not being able to work on documents or deliver final assignments. This could have moderate to disastrous impact; on the other hand, it is fairly unlikely.

The table below displays the assessed impact, probability and significance of each condition discussed above. The impact and probability scales go from 1 to 5, where 1 is lowest and 5 is highest. The significance of an event is then calculated by multiplying the assessed impact and probability of that event. A higher number therefore indicates a greater significance.

| ID | Risk description                               | Impact | Probability | Significance
| -- | ---------------------------------------------- | ------ | ----------- | ------------
| 1  | Lack of cooperation                            | 3      | 3           | 9
| 2  | Unavailable test users                         | 3      | 2           | 6
| 3  | Team members getting sick                      | 3      | 4           | 12
| 4  | Loss of data due to lack of backup             | 5      | 2           | 10
| 5  | Unable to work/deliver due to technical issues | 4      | 2           | 8
    
The table below displays the different events in a risk assessment matrix. The corresponding IDs are marked in the matrix based on the evaluated significance of each scenario.

Green = trivial risk, yellow = intermediate risk, orange = great risk, red = substantial risk

![Matrix](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/raw/master/Vision%20Document/Risk%20matrix.png "Matrix")


## 5.4 Cost, pricing and benefits
|                      | Number of people | Hourly salary (NOK) | Hours per person | Total salary per role (NOK)
| -------------------- | ---------------- | ------------------- | ---------------- | ---------------------------
| Project planning     | 5                | 1470                | 24               | 176400
| Programming          | 5                | 1470                | 30               | 220500
| Documentation        | 5                | 1470                | 40               | 294000
| User test management | 5                | 1470                | 16               | 117600
| Total                |                  |                     |                  | 808500


# 6. Product features

| ID  | Features
| --- | --------
| 1   | Read tasks data from local files
| 2   | Display main menu with program options
| 3   | Option for adding a new task
| 3.1 | Users types in relevant information for one specific task
| 3.2 | User is prompted that task nr.X is registered, and returned to main menu
| 4   | Option for displaying all tasks
| 4.1 | All tasks registered both in files and in program are displayed chronologically
| 4.2 | User is returned to main menu
| 5   | Option for marking a task as finished
| 5.1 | User is prompted for which task number they would like to finish
| 5.2 | User is prompted that specified task nr. is now completed
| 5.3 | User is returned to main menu
| 6   | Option for changing the priority of a task
| 6.1 | User is prompted to enter the task number they would like to re-prioritize
| 6.2 | User is prompted to enter new priority for specific task
| 6.3 | User is prompted that specified task now has updated priority
| 6.4 | User is returned to main menu
| 7   | Option for reordering tasks
| 7.1 | All tasks previously registered are displayed chronologically
| 7.2 | User is prompted to enter which task they would like to re-order
| 7.3 | User is prompted that the specified task is now in requested position
| 8   | Option for quitting program
| 8.1 | Write tasks data to local files


# 7. Requirements
## 7.1 Product
To ensure high usability and a good user experience, the application must relate to Don Norman’s principles of interaction design. They are as follows:

* **Visibility**: The user is able to clearly see what to do and when to do what. For example, this means it is easy for the user to create, edit and delete a new task.
* **Feedback**: The user receives a confirmation whenever an action is completed. For example, the user knows when a new task has been created, and that it was created successfully.
* **Affordance**: The user is able to understand how to complete different tasks. This means it is the relationship between what something looks like and how it’s used.
* **Mapping**: The user is able to use specific controls (keys) to a desired/logical effect. For example, if the user presses ‘N’ at the main menu, it is expected that a new task will be created.
* **Constraints**: The user is restricted to specific interactions at a given moment. For example, the user cannot at any given time delete a task; it has to be created before this is possible.
* **Consistency**: The user is able to perform similar operations in similar interfaces. In addition, the same action has to cause the same reaction, every time.

The product is also required to be designed according to WCAG 2.1 principle 1 - Perceivable.
* 1.2.9 Audio Only (Live)
* 1.3.1 Info and relationships
* 1.3.2 Meaningful sequence
* 1.3.5 Identify input purpose

## 7.2 Documentation
The required documentation for this project will be uploaded to our GitLab under a main WIKI page:
* Vision document
* Wireframes
* Domain model
* Requirements documentation WIKI
* Main report
* Collaboration agreement
* Project plan
* Gantt chart
* Timesheets with status reports
* Meeting invitations
* Meeting minutes
* Final report
* User tests
* Evaluation of teamwork
* Use case model
* Sequence diagram
* Usability
* Universal design
* Testing

Deliverables for this project will be handed in on Blackboard.


# 8. References
Doist Inc - Homepage Todoist application - Read date (03.03.21)
Available at: https://todoist.com/

TickTick Team - Homepage TickTick application - Read date (03.03.21)
Available at: https://www.ticktick.com/

Microsoft - Homepage Microsoft To Do application - Read date (03.03.21)
Available at: https://todo.microsoft.com/tasks/

W3C - WCAG 2.1 Principle 1 - Perceivable - Read date (04.03.21)
Available at: https://www.w3.org/TR/WCAG21/#perceivable

GitLab (2021, 22.02) - Group 27’s Git repository
Available at: https://gitlab.stud.idi.ntnu.no/sebashes/prog1004_2021_group_27

Amirshahi, S. A. (2015) - Vision document template - Read date (02.03.21)
Available at: https://ntnu.blackboard.com/bbcswebdav/pid-1289299-dt-content-rid-34743848_1/xid-34743848_1

Amundsen, M (2020, 20.03) - Vision document example - Read date (02.03.21)
Available at: https://ntnu.blackboard.com/bbcswebdav/pid-1245077-dt-content-rid-34779638_1/xid-34779638_1
