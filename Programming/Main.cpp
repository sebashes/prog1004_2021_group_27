/**
 *  Main program for team 27.
 *  Quillosaur - a to-do-list application.
 *  Stores all tasks data in local text file, and writes new changes to file
 *  Functionality for adding tasks, displaying tasks, changing task category,
 *  priority, and status, as well as reordering and removing tasks.
 * 
 *  @author Anatoli Moroz
 *  @author Edward Ettesvoll
 *  @author Frederik Simonsen
 *  @author Matias Nordli
 *  @author Sebastian Hestsveen
 *  @file main.cpp
 */


#include <iostream>             //  cout, cin
#include <iomanip>              //  setw
#include <fstream>              //  ifstream, ofstream
#include <string>               //  string
#include <ctime>                //  tm, time_t
#include <list>                 //  list
#include "LesData2.h"           //  Toolbox for reading various data


using namespace std;


/**
 *  Task with name, description, start date, finish date, deadline, status, category and priority.
 */
class Task {
private:
    string name;
    string description;
    int startDate;
    int finishDate;
    int deadline;
    int status;
    int category;
    int priority;

public:
    Task(string name);
    Task(ifstream& inn, const int startDate);
    void   changeCategory();
    void   changePriority();
    void   changeStatus();
    void   displayTask() const;
    string getName();
    int    readFinishDate();
    void   writeToFile(ofstream& ut) const;
};


void changeCategory();
void changePriority();
void changeStatus();
void displayAllTasks();
void displayDate(const int date);
void displayOneTask();
void newTask();
int  readDate();
void readFromFile();
void releaseAllocatedMemory();
void removeTask();
void reorderTasks();
void showMenu();
void writeToFile();


list <Task*> gTask;         ///< Data structure with all the tasks.


/**
 *  Main program
 */
int main(void) {
    char command;

    readFromFile();         // Read stored tasks data from file
    showMenu();             // Displays menu options to the user

    command = lesChar("\nOption");
    while (command != 'Q') {
        switch (command) {
        case 'N':  newTask();          break;
        case 'O':  displayOneTask();   break;
        case 'A':  displayAllTasks();  break;
        case 'S':  changeStatus();     break;
        case 'P':  changePriority();   break;
        case 'C':  changeCategory();   break;
        case 'R':  reorderTasks();     break;
        case 'D':  removeTask();       break;
        default:   showMenu();         break;
        }
        command = lesChar("\nOption (press 'M' for all menu options)");
    }

    writeToFile();            // Writes tasks data from run-time to file
    releaseAllocatedMemory(); // Releases allocated memory during run-time

    return 0;
}


//----------------------------------------------------------------------------------------------------------------
//                                      DEFINITION OF CLASSES:
//----------------------------------------------------------------------------------------------------------------

/**
 *  Run-time constructor for class 'Task'. The constructor is used to create new tasks.
 *
 *  @param name - Unique task name 
 *  @see  readDate()
 *  @see  Task::readFinishDate()
 */
Task::Task(string name) {
    
    this->name = name;
    cout << "\nTask description: ";
    getline(cin, description);

    category = lesInt("Task category - 1: Work, 2: Home, 3: Social, 4: Hobby", 1, 4) - 1;
    priority = lesInt("Task priority - 1: High, 2: Medium, 3: Low", 1, 3) - 1;
    status = lesInt("Task status - 1: Not started, 2: In progress, 3: Finished", 1, 3) - 1;

    finishDate = 0;
    startDate = 0;
    deadline = 0;

    if (status != 0) {      // If task is not marked as 'not started':
        cout << "\nStart date:\n";
        startDate = readDate();
    }
    if (status == 2) {      // If task is marked as finished:
        cout << "\nFinish date:\n";
        finishDate = readFinishDate();
    }
    cout << "\nDeadline:\n";
    deadline = readDate();
}


/**
 *  Constructor for adding a task from file
 * 
 *  @param  inn  - the file task data is read from
 */
Task::Task(ifstream& inn, const int startDate) {
    this->startDate = startDate;
    inn >> deadline;
    inn >> finishDate;
    inn.ignore();
    getline(inn, name);
    inn >> status;
    inn >> category;
    inn >> priority;
    inn.ignore();
    getline(inn, description);
}


/**
 *  Changes the category of a task.
 */
void Task::changeCategory() {
    category = lesInt("\n1 - Work, 2 - Home, 3 - Social, 4 - Hobby", 1, 4) - 1;

    cout << "\nThe category of '" << name << "' was changed to ";
    switch (category) {     // Displays correct output depending on actual category:
    case 0:  cout << "work.\n";    break;
    case 1:  cout << "home.\n";    break;
    case 2:  cout << "social.\n";  break;
    case 3:  cout << "hobby.\n";   break;
    }
}


/**
 *  Change priority of a task.
 */
void Task::changePriority() {
    char choice = lesChar("\n(H)igh / (M)edium / (L)ow");
    switch (choice) {             // Converts character H/M/L to correct priority value:
    case 'H':  priority = 0;  break;
    case 'M':  priority = 1;  break;
    case 'L':  priority = 2;  break;
    default:   cout << "\nYou did not enter a valid choice (H/M/L).";  break;
    }

    if (choice == 'H' || choice == 'M' || choice == 'L') {  // If valid priority choice:
        cout << "\nThe priority of '" << name << "' was changed to ";
        switch (priority) {                 // Converts integer value to correct output:
        case 0:  cout << "high.\n";    break;
        case 1:  cout << "medium.\n";  break;
        case 2:  cout << "low.\n";     break;
        }
    }
    else {                                    // Invalid priority choice, error message:
        cout << "\nThe priority was not changed.\n";
    }
}


/**
 *  Changes status of a task.
 * 
 *  @see  readDate()
 *  @see  Task::readFinishDate()
 */
void Task::changeStatus() {
    int tDay = 0;
    string tMon, textDay;

    char choice = lesChar("\n(N)ot started / (I)n progress / (F)inished");

    if (choice == 'N' || choice == 'I' || choice == 'F') { // If valid status choice:
        cout << "\nThe status of '" << name << "' was changed to ";
        if (choice == 'N') {                    // If changing status to not started:
            cout << "not started.\n";
            status = 0;
            startDate = finishDate = 0;
        }
        else if (choice == 'I') {               // If changing status to in progress:
            cout << "in progress.\n";
            status = 1;
            finishDate = 0;
            if (startDate == 0) {
                cout << "\nStart date:\n";
                startDate = readDate();
            }
        }
        else {                                     // If changing status to finished:
            cout << "finished.\n";
            choice = lesChar("Do you want to set the finish date to today or an optional date? (T)oday / (O)ptional");
            if (choice == 'T') {      // If user chooses to set finish date to today:
                status = 2;
                time_t ttime = time(0);
                tm* local_time = localtime(&ttime);     
                tDay = (local_time->tm_mday);


                if ((1 + local_time->tm_mon) < 10) { // Converts 1-9 to 01-09 for correct month output:
                    tMon = '0' + to_string(1 + local_time->tm_mon);
                }

                finishDate = stoi(to_string(1900 + local_time->tm_year) + tMon + to_string(tDay));

                if (tDay < 10) {        // Converts 1-9 to 01-09 for correct day output:
                    textDay = '0' + to_string((local_time->tm_mday));
                    finishDate = stoi(to_string(1900 + local_time->tm_year) + tMon + textDay);
                }
                cout << "\nFinish date set to today's date - ";  displayDate(finishDate);  cout << ".\n";
            }
            else if (choice == 'O') {       // If user chooses to set an optional finish date:
                if (startDate == 0) {       // If no start date has been registered:
                    cout << "\nYou need to register a start date before finish date. Start date:\n";
                    startDate = readDate();
                }
                cout << "\nFinish date:\n";
                finishDate = readFinishDate();
            }
            else {                        // If invalid finish date choice:
                cout << "\nInvalid input.\n";
            }
        }
    }
    else {              // Invalid status choice, display error message:
        cout << "\nYou did not enter a valid choice (N/I/F)."
             << "\nThe status was not changed.\n";
    }
}


/**
 *  Displays the current task's information.
 * 
 *  @see  displayDate(...)
 */
void Task::displayTask() const {
    if (!gTask.empty()) {    // If tasks are registered:
        cout << "\n\tName: " << name << "\n\tDescription: " << description << "\n\tCategory: ";
        switch (category) {  // Convert integer values for category to string output:
        case 0:  cout << "Work";    break;
        case 1:  cout << "Home";    break;
        case 2:  cout << "Social";  break;
        case 3:  cout << "Hobby";   break;
        }
        cout << "\n\tPriority: ";
        
        switch (priority) {  // Convert integer values for priority to string output:
        case 0:  cout << "High";    break;
        case 1:  cout << "Medium";  break;
        case 2:  cout << "Low";     break;
        }
        cout << "\n\tStatus: ";
        switch (status) {  // Convert integer values for status to string output:
        case 0:  cout << "Not started";  break;
        case 1:  cout << "In progress";  break;
        case 2:  cout << "Finished";     break;
        }

        if (startDate != 0) {   // If start date has been set:
            cout << "\n\tStarted: ";  displayDate(startDate);
        }

        if (finishDate != 0) {  // If finish date has been set:
            cout << "\n\tFinished: ";  displayDate(finishDate);
        }

        cout << "\n\tDeadline: ";  displayDate(deadline);
    }
}


/**
 *  Gets the name of a task.
 *
 *  @return  the task name
 */
string Task::getName() {
    return name;
}


/**
 *  Reads the finish date of a task.
 *
 *  @return The finish date of a task
 */
int Task::readFinishDate() {
    if (startDate != 0) {   // If start date is registered:

        string str = to_string(startDate), day, month, year;
        int nDay, nMonth, nYear, temp;

        day = str.substr(6, 2);     // Separates day, month and year from a communal string:
        month = str.substr(4, 2);
        year = str.substr(0, 4);

        nDay = stoi(day);           // Converts day, month and year from strings to integers:
        nMonth = stoi(month);
        nYear = stoi(year);

        temp = lesInt("\tYear", nYear, 3000);
        if (nYear == temp) {       // If finish date year equals start date year:
            year = to_string(temp);
            temp = lesInt("\tMonth", nMonth, 12);
            if (temp == nMonth) {  // If finish date month equals start date month:
                if (temp < 10) {   // Converts 1-9 to 01-09 for correct month output:
                    month = '0' + to_string(temp);
                }
                else {
                    month = to_string(temp);
                }

                if (month == "02") {    // Ensures correct number of days depending on different months:
                    nDay = lesInt("\tDay", nDay, 28);
                }
                else if (month == "04" || month == "06" || month == "09" || month == "11") {
                    nDay = lesInt("\tDay", nDay, 30);
                }
                else {
                    nDay = lesInt("\tDay", nDay, 31);
                }
            }
            else {          // If finish date month does not equal start date month:

                if (temp < 10) {    // Converts 1-9 to 01-09 for correct month output:
                    month = '0' + to_string(temp);
                }
                else {
                    month = to_string(temp);
                }

                if (month == "02") {    // Ensures correct number of days depending on different months:
                    nDay = lesInt("\tDay", 1, 28);
                }
                else if (month == "04" || month == "06" || month == "09" || month == "11") {
                    nDay = lesInt("\tDay", 1, 30);
                }
                else {
                    nDay = lesInt("\tDay", 1, 31);
                }
            }
        }
        else                        // If finish date year does not equal start date year:
        {
            year = to_string(temp);

            temp = lesInt("\tMonth", 1, 12);

            if (temp < 10) {    // Converts 1-9 to 01-09 for correct month output:
                month = '0' + to_string(temp);
            }
            else {
                month = to_string(temp);
            }

            if (month == "02") {    // Ensures correct number of days depending on different months:
                nDay = lesInt("\tDay", 1, 28);
            }
            else if (month == "04" || month == "06" || month == "09" || month == "11") {
                nDay = lesInt("\tDay", 1, 30);
            }
            else {
                nDay = lesInt("\tDay", 1, 31);
            }
        }

        if (nDay < 10) {            // Converts 1-9 to 01-09 for correct day output:
            day = '0' + to_string(nDay);
        }
        else {
            day = to_string(nDay);
        }

        finishDate = stoi(year + month + day); // Converts the strings to a final integer

        return finishDate;
    }
}


/**
 *  Writes task data to file.
 * 
 *  @param  ut - the file data is written to
 */
void Task::writeToFile(ofstream& ut) const {
    ut << startDate;
    ut << " " << deadline;
    ut << " " << finishDate;
    ut << " " << name << '\n';
    ut << status;
    ut << " " << category;
    ut << " " << priority << '\n';
    ut << description << '\n';
}



// ---------------------------------------------------------------------------
//                       DEFINITION OF OTHER CLASSES:
// ---------------------------------------------------------------------------



/**
 *  Lets the user choose for which task to change category.
 * 
 *  @see  displayAllTasks()
 *  @see  Task::changeCategory()
 *  @see  Task::getName()
 */
void changeCategory() {
    displayAllTasks();
    
    if (!gTask.empty()) {   // If there are tasks registered:
        int i = 0;
        string choice, temp;
        cout << "\nPress 'X', then ENTER, to go back.";
        cout << "\nOr enter name of task to change category for: ";
        getline(cin, choice);

        for (auto& val : gTask) {
            i++;
            temp = val->getName();
            if (temp == choice) {   // If name matches a task:
                val->changeCategory();
                break;
            }
            else if (choice == to_string(i)) { // If task number matches a task:
                val->changeCategory();
                temp = to_string(i);
                break;
            }
        }
        if (choice == "x" || choice == "X") { // If users regrets trying to change a task category:
            cout << "\nReturning to main menu.\n";
        }
        else if (temp != choice) {  // If name entered does not match an actual task name:
            cout << "\nInvalid task name.\n";
        }
    }
}


/**
 *  Lets the user choose for which task to change priority.
 * 
 *  @see  displayAllTasks()
 *  @see  Task::changePriority()
 *  @see  Task::getName()
 */
void changePriority() {
    displayAllTasks();
    
    if (!gTask.empty()) {   // If there are registered tasks:
        int i = 0;
        string choice, temp;
        cout << "\nPress 'X', then ENTER, to go back.";
        cout << "\nOr enter name of task to change priority for: ";
        getline(cin, choice);

        for (auto& val : gTask) {
            i++;
            temp = val->getName();
            if (temp == choice) {   // If name matches a task name:
                val->changePriority();
                break;
            }
            else if (choice == to_string(i)) { // If number matches a task number:
                val->changePriority();
                temp = to_string(i);
                break;
            }
        }
        if (choice == "x" || choice == "X") {   // If user regrets trying to change task priority:
            cout << "\nReturning to main menu.\n";
        }
        else if (temp != choice) {  // If name entered does not match an actual task name:
            cout << "\nInvalid task name.\n";
        }
    }
}


/**
 *  Lets the user choose for which task to change status.
 * 
 *  @see  displayAllTasks()
 *  @see  Task::changeStatus()
 *  @see  Task::getName()
 */
void changeStatus() {
    displayAllTasks();

    if (!gTask.empty()) { // If there are registered tasks:
        int i = 0;
        string choice, temp;
        cout << "\nPress 'X', then ENTER, to go back.";
        cout << "\nOr enter name of task to change status for: ";
        getline(cin, choice);

        for (auto& val : gTask) {
            i++;
            temp = val->getName();
            if (temp == choice) { // If name equals actual task name:
                val->changeStatus();
                break;
            }
            else if (choice == to_string(i)) { // If number equals actual task number:
                val->changeStatus();
                temp = to_string(i);
                break;
            }
        }
        if (choice == "x" || choice == "X") { // If user regrets trying to change a task status:
            cout << "\nReturning to main menu.\n";
        }
        else if (temp != choice) {  // If name does not match an actual task name:
            cout << "\nInvalid task name.\n";
        }
    }
}


/**
 *  Displays all registered tasks.
 * 
 *  @see  Task::displayTask()
 *  @see  Task::getName()
 */
void displayAllTasks() {
    int nr = 0;
    if (!gTask.empty()) {   // If there are registered tasks:
        cout << "\nRegistered tasks:";
        for (auto& val : gTask) {
            cout << "\nTask nr. " << setw(2) << ++nr << ": " << val->getName();
        }
        cout << '\n';
    }
    else {                  // Error message if there are no tasks registered:
        cout << "\nNo existing tasks.\n";
    }
}


/**
 *  Display date to user in a tidy manner.
 * 
 *  @param  date  - The date to display
 */
void displayDate(const int date) {
    string str = to_string(date), day, month, year;

    day   = str.substr(6, 2);   // Divides a string into three separate strings for day, month and year:
    month = str.substr(4, 2);
    year  = str.substr(0, 4);

    cout << day << ' ';

                                // // Converts number to name of actual month:
    if (month == "01")       cout << "January";
    else if (month == "02")  cout << "February";
    else if (month == "03")  cout << "March";
    else if (month == "04")  cout << "April";
    else if (month == "05")  cout << "May";
    else if (month == "06")  cout << "June";
    else if (month == "07")  cout << "July";
    else if (month == "08")  cout << "August";
    else if (month == "09")  cout << "September";
    else if (month == "10")  cout << "October";
    else if (month == "11")  cout << "November";
    else                     cout << "December";

    cout << ", " << year;
}


/**
 *  Displays detailed information about one particular task.
 * 
 *  @see  displayAllTasks()
 *  @see  Task::displayTask()
 *  @see  Task::getName()
 */
void displayOneTask() {
    displayAllTasks();
    if (!gTask.empty()) {   // If there are tasks registered:
        int i = 0;
        string choice, name;
        auto it = gTask.begin();

        cout << "\nEnter name of task to display: ";
        getline(cin, choice);
        
        for (auto& val : gTask) {
            i++;
            name = val->getName();
            if (name == choice) {   // If name equals actual task name:
                val->displayTask();
                cout << "\n";
                break;
            }
            else if (choice == to_string(i)) {  // If number equals actual task number:
                val->displayTask();
                cout << "\n";
                name = to_string(i);
                break;
            }
        }
        if (name != choice) {   // If name does not equal an actual task name:
            cout << "\nInvalid task name.\n";
        }
    }
}


/**
 *  Create a new task.
 * 
 *  @see  Task::Task()
 *  @see  Task::getName()
 */
void newTask() {
    bool nameMatch = false;
    string temp, name;
    
    cout << "\nPress 'X', then ENTER, to go back.";
    cout << "\nOr enter name of new task to create: ";
    getline(cin, name);

    if (name != "x" && name != "X") {   // If user does not regret to add a new task:
        for (auto& val : gTask) {
            temp = val->getName();
            if (temp == name) { // If task name already exists:
                nameMatch = true;
                break;          // Exits function to avoid duplicate task names
            }
        }

        if (!nameMatch) {   // If task name does not exist:
            gTask.push_back(new Task(name)); // Creates a new task and adds the task to the list
        }
        else {  // If duplicate task name:
            cout << "\nThe task name already exists.\n";
        }
    }
    else {  // If user regrets adding a new task:
        cout << "\nReturning to main menu.\n";
    }
}


/**
 *  Reads day, month and year from user.
 *
 *  @return  date as int
 */
int readDate() {
    string day, month, year, date;
    int temp;

    // Reading year
    temp = lesInt("\tYear", 2000, 3000);
    year = to_string(temp);

    // Reading month
    temp = lesInt("\tMonth", 1, 12);
    month = to_string(temp);
    if (temp < 10)  month = '0' + month; // Converts 1-9 to 01-09 for correct month format

    // Reading day (day numbers allowed varies depending on actual month):
    if (month == "02") {
        temp = lesInt("\tDay", 1, 28);
    }
    else if (month == "04" || month == "06" || month == "09" || month == "11") {
        temp = lesInt("\tDay", 1, 30);
    }
    else {
        temp = lesInt("\tDay", 1, 31);
    }
    day = to_string(temp);
    if (temp < 10)  day = '0' + day; // Converts 1-9 to 01-09 for correct day format

    date = year + month + day;
    return temp = stoi(date);   // Convert string to int.
}


/**
 *  Reads data from file.
 * 
 *  @see  Task::Task(...)
 */
void readFromFile() {
    ifstream infile("DATA.DTA");  int startDate;

    if (infile) {   // If able to open text-file:
        cout << "Importing task data ...\n";
    
        infile >> startDate;
        while (!infile.eof()) {
            gTask.push_back(new Task(infile, startDate)); // Adds task from file to list
            infile >> startDate;
        }
        infile.close();
    }
    else {      // If no text-file exits:
        cout << "No previous data saved ...\n";
    }
    cout << "\n\tWelcome to Quillosaur - your go-to to-do list program!"
         << "\n\tCreate, display, edit, reorder and delete tasks as you wish."
         << "\n\tAll task data is automatically saved when you quit the program.";
}


/**
 *  Releases ALL allocated memory under run-time.
 */
void releaseAllocatedMemory() {
    while (!gTask.empty()) {
        delete gTask.front();
        gTask.pop_front();
    }
}


/**
 *  Removes a chosen task.
 * 
 *  @see  displayAllTasks()
 *  @see  Task::getName()
 */
void removeTask() {
    string choice, temp;
    int i = 0;
    displayAllTasks();
    if (!gTask.empty()) {   // If there are registered tasks:
        auto it = gTask.begin();

        cout << "\nPress 'X', then ENTER, to go back.";
        cout << "\nOr enter name of task to delete: ";
        getline(cin, choice);

        for (auto& val : gTask) {
            i++;
            temp = val->getName();
            if (temp == choice ) {  // If name entered matches actual task name:
                delete* it;
                gTask.erase(it);
                cout << "\nTask deleted successfully.\n";
                break;
            }
            else if (choice == to_string(i)) { // If number entered matches actual task number:
                delete* it;
                gTask.erase(it);
                cout << "\nTask deleted successfully.\n";
                temp = to_string(i);
                break;
            }
            
            it++;
        }
        if (choice == "x" || choice == "X") {   // If user decides to not remove a task:
            cout << "\nReturning to main menu.\n";
        }
        else if (temp != choice) {  // If name entered does not match actual task name:
            cout << "\nInvalid task name.\n";
        }
    }
}


/**
 *  Allows the user to move a desired task to a new position in the list
 *
 *  @see  displayAllTasks()
 *  @see  Task::getName()
 */
void reorderTasks() {
    if (gTask.size() > 1) { // If more than one task is registered:
        string name, choice;
        int counter1 = 0, counter2 = 0, i = 0;
        displayAllTasks();
        auto it1 = gTask.begin();
        auto it2 = gTask.begin();

        cout << "\nPress 'X', then ENTER, to go back.";
        cout << "\nOr enter name of task to move: ";
        getline(cin, choice);

        for (auto& val : gTask) {
            i++;
            name = val->getName();
            if (name == choice) {   // If name entered matches actual task name:
                int pos = lesInt("Enter position to move it to", 1, gTask.size())-1;
                for (int i = 0; i < pos; i++) {
                    it1++;
                    counter1++;   
                }
                if (counter1 > counter2) {
                    it1++;
                }
                gTask.insert(it1, val);
                gTask.erase(it2);
                displayAllTasks();
                break;
            }
            else if (choice == to_string(i)) { // If number entered matches actual task number:
                int pos = lesInt("Enter position to move it to", 1, gTask.size()) - 1;
                for (int i = 0; i < pos; i++) {
                    it1++;
                    counter1++;
                }
                if (counter1 > counter2) {
                    it1++;
                }
                gTask.insert(it1, val);
                gTask.erase(it2);
                displayAllTasks();
                name = to_string(i);
                break;
            }
            it2++;
            counter2++;
        }
        if (choice == "x" || choice == "X") { // If user regrets to move a task:
            cout << "\nReturning to main menu.\n";
        }
        else if (name != choice) {  // If name entered does not match an actual task:
            cout << "\nInvalid task name.\n";
        }
    }
    else {  // If there is less than two tasks registered:
        cout << "\nNot enough tasks added.\n";
    }
}


/**
 *  Displays all menu options to the user.
 */
void showMenu() {
    cout << "\n\nAVAILABLE OPTIONS:\n"
        << "\t(N) - New task\n"
        << "\t(O) - One task in detail\n"
        << "\t(A) - All tasks summarized\n"
        << "\t(S) - Status change\n"
        << "\t(P) - Priority change\n"
        << "\t(C) - Category change\n"
        << "\t(R) - Reorder tasks\n"
        << "\t(D) - Delete a task\n"
        << "\t(Q) - Quit program\n";
}


/**
 *  Write data to file.
 * 
 *  @see  Task::writeToFile(...)
 */
void writeToFile() {
    ofstream outfile("DATA.DTA");

    cout << "\n\nExporting task data ...";
    for (auto& val : gTask) {
        val->writeToFile(outfile); // Writes task data to file
    }
    cout << "\n\n";
}
