# Quillosaur - To-Do-List Application

Welcome to the repository of 'Quillosaur' - an efficient and user-friendly to-do list application.


├───Class Diagram  
├───Collaboration Agreement  
├───Domain Model  
├───Final report  
├───Installation manual  
│   └───Images  
├───Main Report  
├───Meetings  
│   ├───Meeting summons  
│   └───Minutes of meeting  
├───Program testing  
├───Programming  
├───Project Plan  
├───Project Structure  
├───Sequence Diagram  
├───Time Sheets  
├───Use-case diagram  
├───User manual  
│   └───Images  
├───User testing  
├───Vision Document  
└───Wireframes

Above is an overview of the project structure. A more detailed explanation/illustration of the project structure can be found [**here**](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/wikis/Project-Structure).

[**Installation manual**](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/wikis/Installation-Manual)  
[**User manual**](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/wikis/User-Manual)  
[**Source code**](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/tree/master/Programming)  
[**Wiki**](https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/wikis/home)
