# Main Report
Edward Ettesvoll, Sebastian Hestsveen, Anatoli Maroz, Matias Nordli, Frederik Simonsen


[[_TOC_]]

# Revision history
| Date           | Version | Description
| -------------- | ------- | -----------
| 21 April, 2021 | 0.1     | Initial draft
| 29 April, 2021 | 1.0     | Final draft


# 1. Preface
The main report is linked to the course "PROG1004 - Software Development", at NTNU in Gjøvik. It is part of the team's documentation work on the project assignment in this course.


# 2. Introduction
This document provides an overview and summary of the project. It details how the team work progressed, whether effort and result match what was initially planned, and what the team is most pleased with. It also explains what the team would have done differently, source code and documentation the team is particularly satisfied with, as well as the experiences the team has had with using different collaborative tools.

The purpose of this document is to summarize experiences from working on the project, as well as how the cooperation has been within the team.


# 3. Assignment description
The following description is taken directly from the project assignment:

"Your task is to create a to-do-list application. The user should be able to register new tasks in the application. When the task is added, the application should keep all relevant information like task description, priority, category, status, deadline, and start and finish date.

The user should also be able to mark tasks as done, change the priority of the task and reorder tasks. The requirements explained above are not final. Additional functions and features should be added according to feedback you get from the tests and other feedback."

In other words, the task is to create a full-fledged piece of software/program from scratch, as opposed to maintaining something that already exists. The client involved was as follows:

**Seyed Ali Amirshahi**  
_Associate Professor, NTNU_  
Department of Computer Science  
Faculty of Information Technology and Electrical Engineering  
E-mail: s.ali.amirshahi@ntnu.no  
Phone: 916 34 408  

The client also functioned as an expert advisor.


# 4. Theory
All students in the group are first year students at NTNU Gjøvik and are studying for a Bachelor of Science in digital infrastructure and cybersecurity. For this project we have used theory and principles from the following subjects, who were all taught at the university: 

* PROG1001 - Fundamental programming
* PROG1003 - Object-oriented programming
* PROG1004 - Software Development and  
* DCSG1002 - Cybersecurity and teamwork 

The lectures and lecturers in the aforementioned subjects have provided us with relevant guidelines and theory to use in the implementation of this project. The programming courses have taught us both basic programming in C and C++, while the two other subjects have taught us how to work together as a team, and provided us with insights into documentation, work flow and process, and both plan-driven and agile software development. 

## 4.1 User testing
User testing is used to gain the proper knowledge you need to develop systems or programs and increase their respective usability. User tests are conducted to ensure that existing solutions work, and you gain valuable insight and what needs to be improved to make your product even better and more user-friendly. It helps you develop an understanding of what your target audience is looking for and it is important to test on a broader demographic, since different people will have different backgrounds, knowledge, and experience. By starting user testing early, you can uncover flaws in your initial design and develop a clearer and more concise plan on how to improve your product for future iterations. It is important to both plan a user test, as well as designing the user tests with a clear goal in mind on what you would like to achieve.

For this project we have focused on qualitative studies and we wanted to test on people varying in age, profession, and IT-experience. While developing our software we had a few success criteria in mind. It was important to us to develop a program that was efficient, easy to use and learn, and user-friendly. The program requires little navigation, and we have focused on creating a uniformity in format in our different functions. We wanted this to be reflected in our user-testing, and during the planning process of our user tests, our main goal was to find out how the program’s usability could be improved and investigate if the program appealed to a larger target audience. We recruited family members and friends to answer our tests, and we fulfilled our requirement to have a user testing group varying in age and professional experience. All our test users may be considered as non-expert users, as none of the test users have any software development experience. The tests themselves were conducted remotely without supervision. For the early stages of the testing process, the user group answered surveys regarding our wireframes. When we conducted user tests on the MVP, they were done via screen share and with an observer.  

After different iterations of the user testing concluded it was important that we analyzed the data obtained and focused on critical and frequently mentioned feedback regarding certain functions. The knowledge gained from the user tests was immensely valuable to the team. It helped us with improving our final program in a more user-friendly matter with focus on what actual users would like the program to achieve and do. User testing overall helped us uncover usability issues, gather true feedback from the target audience, and in return improve end-user satisfaction. We have reported all our findings and conclusions from the user testing in our repository on GitLab, where it is possible to read our summaries. 

## 4.2 Prototyping
Prototyping refers to a presentation of a product and is used for development, testing and evaluation purposes. A prototype allows you to experiment with different approaches, capture design concepts and test on potential end-users. Prototyping and user testing in cohesion provides the development team with valuable feedback from users, and in return they get a better understanding of what potential users would like to see in a product. It opens a form of communication between users and developers. In the early stages of a development process, you can test different aspects of a product, regarding functionality, content, appearance, and interactivity. With prototyping you can check the program’s compatibility with users, and it helps you build a solid foundation of knowledge which you can use to create new ideas for improvements. Early prototypes allow you to configure changes early, which saves money, time, and effort. Prototypes also helps the development team better understand how stakeholders and users feels about the product they are developing, and if it fits a need in a potential market.

For this project we have used both low- and high-fidelity prototypes. Fidelity refers to the level of detail and functionality provided in different types of prototypes. In the early and mid-stages of the project we have created different versions of wireframes to use as low-fidelity prototypes. The wireframes are easy to make and update, and they give the users a quick overall view of the program. The downside to low-fidelity wireframes in general is that they can lack a certain realism and interactivity for users. The users themselves must imagine how to use the product. For the high-fidelity prototype we created an MVP, which is a minimum viable working version of the final program being developed. The MVP is more engaging and interactive for testing and gives the users a more realistic feel of how the final product will be. The downside is that they are more time consuming to create, which in return can make the development team more resistant to implement necessary changes and improvements to the program.

Throughout the different stages of prototyping in this project, the team has been able to gain valuable knowledge and realize different solutions to improve the final product being presented to users. The feedback from users both regarding user testing and prototyping has been very valuable to the team and has helped us to create a more user-friendly and accessible program. By making early changes based on feedback, we were able to correct minor issues.  This led to both time and effort saved in comparison to making changes later in the development process. It helped us build a product that better meet end-user requirements and convert our ideas into source code. 

## 4.3 Universal Design
Universal Design can be described as the design and composition of an environment or a product so that it may be accessed, understood, and used to the greatest possible extent by as many people or users as possible. By adhering to Universal Design principles, you can make your electronics-based products more accessible to people with any physical, sensory, mental health or intellectual disability, which in return makes your program easier to use and learn for anyone. It is important to focus on different users when you design a product, both to include as many people and users as possible and design your product in a way to minimize the difficulties of adaptation to different users. One is not expected to find a solution that accommodates the needs of everyone, but it is important to incorporate solutions that are more inclusive, without compromising the integrity and quality of the product. Designers should see the use of Universal Design as an opportunity to analyze their product as a whole and provide a more equivalent experience for everyone. 

In this project we were tasked to design our program according to WCAG (Web Content Accessibility Guidelines) 2.1 principle 1 – Perceivable. The guidelines in this principle emphasizes that you must provide text alternative for any non-text content, provide alternatives for time-based media, create content that can be presented in different ways without losing information or structure, and make it easier for users to see and hear content. We feel that our program adheres to most of these guidelines, as we have created a text-based program, without any non-text content and time-based media. The text is easier to see since we have used black as the background color with white text, which in return yields a high contrast ratio. We feel that the information, structure, and relationships in the program are clearly conveyed in a text-format, and the program itself has a meaningful sequence. It is easy to orientate where you are in the program, and input purposes are clearly defined. The program we have created is easily accessible for many users, and it is easy to use for most people. One can always make improvements, and our program is not suited for people who are blind, as we have not incorporated any braille functionality. 

We have also focused on staying true to the principles provided by the Center for Universal Design at North Carolina State University. It was important to us to make a simple and intuitive program to use, so it could be understood by different regardless of experience and knowledge. The program conveys understandable necessary information to the user, and we have increased the program’s error tolerance by minimizing unintended actions by incorporating duplicate checks and regret functionality in different functions. Our design is usable and accessible to people with different skills and the users are required to make minimal physical effort to efficiently operate the program. The program’s text formatting also provides clear and concise displays and instructions for users to follow.


# 5. How the assignment was solved

## 5.1 Methods and standards used
Our assignment for this project was to create a standalone to-do-list application in C++, which keeps persistent data in a local text file, and we had to document our entire process during the project. To solve this assignment, we have gone through multiple different aspects, methods, and standards regarding software development. The project has been split up in three iterations, and different deliverables were expected at different times during the project. Several of the models, documents, and the source code itself has been continuously updated and improved during the duration of the project. 

During the first iteration the team started with defining roles and overall goals for the team’s performance and grade. A collaboration agreement was made to establish rules and roles within the team. We started having weekly meetings with our teaching assistant to both brainstorm, problem solve and ask any questions the group might have for the teaching assistant. This was also done to ensure a consistent workflow and progress throughout the project. A vision document was formed to clarify the product’s positioning, project goals for the group, and provide a stakeholder, user, and product overview. Wireframes were created to represent a rough sketch of what the final product would look like and gave us a chance to start with early usability tests. A domain model was created to generally describe the entities and relationships in our to-do-list application. The group also started preliminary programming and worked towards pushing a running and always working version of the source code to Git. An issue board and a requirements WIKI was created on Git, so the group could keep track of issues to be solved and the WIKI was implemented so users could navigate the repository in a more interactive and accessible way. 

In the second iteration the group implemented a Gantt chart to track deadlines, milestones, and work progress across team members.  A sequence diagram was formed to show a visual representation of the communications between the user, program, container, and text file. The group analyzed the data obtained from early usability tests and created a user testing report from the first iteration. A use-case diagram was made to cover all the actions the user can do while using our program. The group continuously worked on improving the source code of the program based on feedback from usability tests, and changes were made to the wireframes to meet user expectations. Improvements were made to WIKI structure on Git, and the group also presented a MVP to the client, as well as conducting usability tests on both the MVP and improved wireframes. 

For the final iteration, the group created a class diagram to provide a listing of our task-class from the program and list out variables and functions contained in the class. Updates and improvements were made to all previously mentioned documentation. A new user test report based on the feedback from the MVP and updated wireframes was written, and both a user and installation manual were created. The group worked in collaboration to finish the main report, team evaluation and filling out test forms as learned in object-oriented programming. The program itself was finished with all desired functionality implemented. A project structure was created to show how the repository is split into different directories with corresponding documentation.

For the duration of the project, the group has written meeting summons and minutes of meeting. These have continuously been uploaded to the Git repository. A timesheet has also been used to keep track of each member’s time used on the project and provided a short description for each member for what has been done.

## 5.2 Use of literature and Internet
Most of the project work has been based on knowledge gained in lectures in both the software development and object-oriented programming courses. There has been provided lecture notes in the software development course which has provided us with relevant knowledge for the whole software engineering process. Fundamentals in software engineering, usability, evaluation, modelling, software architecture, system development methodology, testing and documentation have been described in these lecture notes. The video recordings and documentation in programming courses has taught us how to follow programming fundamentals, write non-redundant code, comment properly and work with standard template libraries.

If there ever were any doubts for the group on how to proceed with a specific task in the project, our teaching assistant was consulted in the weekly meetings. The course coordinator has also been contacted directly on a few occasions, to clarify different assignment descriptions. The group has also used Internet searches throughout the project, especially when it comes to different models and diagrams that were to be created. This was done to look at other examples of these models, to have a better understanding on how to proceed with our own work. This made it clearer and how these models and diagrams should be implemented for this specific assignment. Other relevant searches were also done in case the group were insecure in how to proceed with a specific document, to gain relevant knowledge and experience for writing documentation. 

## 5.3 Hardware
For this project we have used our own personal computers and laptops. To run the program you need to use a Windows computer or laptop with ordinary specifications. This is due to the reading from file function will not skip lines correctly in the text-file on Linux or Mac. The only requirement for working with this project was to have a functioning computer that could run the softwares listed below. 

## 5.4 Software
The software we chose to work with varied on the different group member, as many of them relied on preference. For instance, everyone would need to use an internet browser to access [NTNU's GitLab](https://gitlab.stud.idi.ntnu.no), but a specific browser was not required. Some of the browsers used were [Google Chrome](www.google.com/chrome), [Firefox](https://www.mozilla.org/en-US/firefox/) and [Opera](https://www.opera.com/no). We also used [Powershell Core](https://github.com/PowerShell/PowerShell/releases/tag/v7.1.3) and [Git BASH](https://gitforwindows.org) to work further with Git. 

For programming, we would all need a code editor. Some of the editors used were [Visual Studio 2019](https://visualstudio.microsoft.com/vs/), [Visual Studio Code](https://code.visualstudio.com) and [Code::Blocks](https://www.codeblocks.org). Everyone used [Draw.io Diagrams](https://drawio-app.com) to create diagrams such as the Domain Model, Sequence Diagram and Use Case Diagram. We also used [Google Drive](https://drive.google.com/drive/folders/1IMI1fyvG1N7qk4OtwN1jcItC7LvvvWHb?usp=sharing) to edit our timesheet and presentations. This was also used for the collaboration agreement and vision document in the beginning of the project, but we later switched this to Git aswell. To create virtual work environments, the group members used [SkyHigh](https://skyhigh.iik.ntnu.no/horizon/project/) with Windows' Remote Desktop Connection and [VirtualBox](https://www.virtualbox.org).

## 5.5 How the work was divided between the team members
Throughout the project team members have assisted each other when needed, and some of the major documentations have been written in a collaborative effort. It has been important for the team that we play to each other strengths, and make sure that our final product is as good as it can be. The whole group has worked on the collaboration agreement, vision document, main report, and the final report. The team members have also done some assignments mostly on their own, and we will provide a list below of what the different team members have done during the project:

Anatoli has had mainly documentation responsibilities. He has written minutes of meeting for all the meetings he has participated in. The different user test reports have been his responsibility and he has explained the methodologies used during the user tests and summarized our key findings and results. He constructed the Gantt chart for the group, which shows our workflow during different iterations of the project, and who has been working on what during different times. In the final stages of the project, he wrote the installation manual for our program, so any user that would like to try to run our program has a guide they can use to set up an IDE to run Quillosaur.

Edward has been responsible for the Git WIKI structure and his main role has been as the document manager. As a document manager, he has been responsible for reviewing any documentation presented by the group, and make sure that we respect any deadline and hand in all the deliverables on time. He has written about Universal Design and project structure on the WIKI and been in control of the issue board on Git. He has also contributed to the source code and the program written by the group.

Frederik has acted as the meeting leader in the group and has written meeting summons for all meetings with the teaching assistant or client.  He created the use-case diagram, which covers all the actions the user can do while running the program. Throughout the project he has created different versions of the wireframes and made necessary changes to these based on both user and stakeholder feedback. On the Git WIKI, he has written about persistence. Other than what has been described above, he has mainly had documentation responsibilities. 

Matias has been the project leader for the group and has acted as a mediator if any conflicts has arisen within the team. He has had a larger overview of all assignments within the group and made sure that we have kept steadily progressing throughout the project. Most of the different models created have been his work, as he made the domain model, sequence diagram, and class diagram. He contributed to the source code and helped the system developer plan the program and how it should be structured.

Sebastian has acted as the system developer during the project. He has written most of the source code, and continuously developed the program’s functionality based on user feedback and the project assignment. He was responsible for the MVP and presented this to the client during our second meeting. Debugging and testing of the program has also been his area of focus. For documents presented by the group, he has written the user manual and handled most of the program testing forms.

## 5.6 An overview of the documentation
Here is an overview of all the documentation provided by the group during the project:
- Class diagram
- Collaboration agreement
- Domain model
- File structure
- Final report
- Gantt chart
- Installation manual
- Issue board
- Main report
- Meeting summons
- Minutes of meeting
- Persistence
- Program testing
- Project structure
- Sequence diagram
- Source code
- Timesheets
- Universal Design
- Usability tests
- Use-case diagram
- User manual
- User test reports
- Vision Documents
- WIKI documentation
- Wireframes


# 6. Implementation of the project

## 6.1 Planning, work distribution and progress
One of the things the group wishes could be made a bit different is the early planning that includes the Gantt diagram. We did not spend as much time on the planning phase as we perhaps should, and we have not clarified how time should be submitted in the time log and rationally evaluated our effort from early stages. That caused some problems and misunderstandings in the group as things progressed but were resolved by revisioning the plan and analyzing and revaluating the work done.

Besides, the distribution of work went well.  Each milestone, or iteration in our case, had different processes that should be distributed within the group. After we got the objectives for each milestone, we distributed the work to each group member. Each of us had different roles from the beginning of the project, and the routine processes were distributed accordingly. Meeting summons and meeting summaries had permanent assignees since this is what the group thought was productive. Same things applied to the people who made models, user tests, user test reports and so on. The group decided that the flexibility of changing roles was not necessary, and the members should rather get more experienced at what they do. 

To analyze the successfulness of the goals for the project, we should examine the execution of processes in the Gantt diagram. All processes for each iteration were completed with the different start and end dates. Some of the processes had vague start and end dates and thus set to the start and end dates of the corresponding iteration or phase. The final Gantt chart should have all process lines gray since the group has finished all of them. 

## 6.2 Collaboration agreement in practice 
The group have achieved both the effect goals and result goals described in the collaboration agreement. We have taken advantage of individual skills, to provide a more complete and creative final product. The teamwork has been flexible, and group members has extended their reach to assist other team members facing any difficulties. Our experience and knowledge have developed throughout the project, and we can now better describe the fundamentals of software development to others. Meetings have been conducted every week as agreed, with two exceptions: Easter and DevOps. Documents have been continuously pushed to Git, but the group has changed from using Google Docs to working directly in markdown and Git. Quality control has been done mostly by the group itself, but the group has asked the teaching assistant for help in specific cases where we were unsure on how to proceed. All deadlines have been respected by the team, and the deliverables have been handed in on time. During some team meetings, some group members have been missing, but group members have never missed two meetings in a row, hence fulfilling the terms described in the contract. All meeting participants have been active and present during the meetings. The team has done their best to support each other with different assignments. Both the team roles and collaboration have been flexible and functional. The group had a minor conflict in the early stages of the project, but this was handled according to the agreement. The team tried to resolve the issue internally first, and after that the teaching assistant was involved. This resolved the conflict, and the team member in question upped their performance and commitment.

## 6.3 Hours
In the vision document, our team set up a plan for predicted hours used per person on each aspect of the project. These hours illustrated what each team member would use on average - that is, after combining the hours of all five individuals and then dividing by five. The numbers were as follows:
- Project planning: 24 hours
- Programming: 30 hours
- Documentation: 40 hours
- User test management: 16 hours

The team ended up spending significantly less time on project planning and user test management, with most of the alloted time spent on programming and documentation. As a result, a more realistic distribution of hours would look like this:
- Project planning: 10 hours
- Programming: 40 hours
- Documentation: 55 hours
- User test management: 5 hours

It is worth noting that the different team members did not have the same distribution of hours across the same activities. For example, while certain team members ended up spending more time on documentation, others were more invested in programming. Similarly, project planning (mainly meeting summons and meeting summaries) was largely handled by two individuals on the team.

## 6.4 How have the technical issues been resolved?
The technical issues that have arisen during our coding, has been solved in debugging mode in Microsoft Visual Studio 2019. This has helped us isolate problems in our source code and has made it easier to solve one issue at a time. The group also had a tough time in the beginning adjusting to both Git and markdown, compared to using Google Docs for documentation purposes. With more practice and experience over time, we feel that we have adjusted well to using Git and markdown for documenting different assignments. The group has worked with Git and markdown since after the first iteration was delivered.

## 6.5 Use of collaborative tools
The group has worked with a Git repository for all documentation purposes after the first iteration. Prior to this, Google Docs were used to write different documents.  The repository has been implemented with different branches, so that multiple people could work on either the same document or the source code simultaneously. Every team meeting that the group has had, has been held on Discord. For presentations with the client, Microsoft Teams has been used as a communications platform. 

## 6.6 Risk analysis
The team also presented different risks in the vision document. Here, we anticipated that the greatest threat to the success of our project would involve team members getting sick. This was based on the potential consequences of many team members getting sick at the same time, in addition to the ongoing pandemic, but neither of these two factors ended up having too much of an impact.

Loss of data due to lack of backup was the element we considered to be of the second greatest risk. However, in the end, we were not affected much by this either, as the team consistently used platforms/solutions for backing up and storing project data. Perhaps of greater impact was the risk with the third-highest significance, namely lack of cooperation. The team faced some troubles here early in the project phase, where there were discussions around hours necessary to spend on the project to achieve a desired result. However, as adressed in the risk analysis, the team was indeed able to take action early and prevent this from having too much of an impact.

## 6.7 What limits the system?
A lack of a graphical user interface limits the functionality and user experience. There is no multi-user functionality, and tasks cannot be assigned to different persons. If the program had functionality for user login and password, the tasks data could be saved to a cloud storage platform, and there would be easier to have backups of your to-do-list. The possibility to create their own task categories would also enhance the user experience and make the program more customizable and interactive.


# 7. Further work
While our solution solved what the assignement asked for, there are many things that could be done to improve the product. 

First of all, a graphical user interface could enhance the user experience. This would, of course, lead to a huge amount of work involving a complete re-write of our code to a language more suitable for graphical design.

A system with user login could also be implemented. This would allow users to save their tasks in a cloud storage, and therefore be able to use it on multiple devices. A database system with user info would also be required here, where we also could store task data instead of in a local text file. 

One could also implement task folders, where the user could create their own lists of tasks that fall under their own, personal category. And if we were to make the program this complex, a search function would make it way easier for the user to navigate through all of their tasks. This could also include settings where one could filter tasks by category, dates, etc.


# 8. Repository
The team's wiki can be found at: https://gitlab.stud.iie.ntnu.no/sebashes/prog1004_2021_group_27/-/wikis/home
